package br.com.leilao;

public class Leiloeiro {

    private String nome;
    private Leilao leilao;

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance() {
        Lance lanceAux = new Lance();
        for (Lance lance : this.leilao.getLances()) {
            if (lanceAux == null) {
                lanceAux = lance;
            } else if (lanceAux.getValorLance() < lance.getValorLance()) {
                lanceAux = lance;
            }
        }
        return lanceAux;
    }

}
