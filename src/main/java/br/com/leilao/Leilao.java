package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {

    private List<Lance> lances = new ArrayList<>();

    public Leilao() {
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance adicionarNovoLance(Lance lance) {
        if (validarLance(lance)) {
            this.lances.add(lance);
            System.out.println("Lance adicionado! " + lance.getValorLance());
            return lance;
        } else {
            System.out.println("Esse lance é muito baixo! " + lance.getValorLance());
            return null;
        }
    }

    public boolean validarLance(Lance lance) {
        if (this.lances.size() == 0) {
            return true;
        } else if( this.lances.get(this.lances.size()-1).getValorLance() > lance.getValorLance()) {
            return false;
        } else {
            return false;
        }
    }
}
