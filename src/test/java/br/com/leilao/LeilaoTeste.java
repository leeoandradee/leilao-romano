package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LeilaoTeste {

    public Usuario user1 = new Usuario(1, "Leonardo");
    public Lance lance;
    public Leilao leilao = new Leilao();

    @Test
    public void testarAdicionarLance() {
        lance = new Lance(user1, 100.0);
        Lance lanceAdicionado = leilao.adicionarNovoLance(lance);
        Assertions.assertEquals(100.0, lanceAdicionado.getValorLance());
        Assertions.assertEquals("Leonardo", lanceAdicionado.getUsuario().getNome());
        Assertions.assertEquals(1, lanceAdicionado.getUsuario().getId());
    }

    @Test
    public void testarAdicionarErro() {
        lance = new Lance(user1, 100.0);
        Lance lance1 = new Lance(new Usuario(2, "Roger"), 200.0);

        Lance lanceAdicionado1 = leilao.adicionarNovoLance(lance1);
        Lance lanceAdicionado2 = leilao.adicionarNovoLance(lance);

        Assertions.assertEquals(null, lanceAdicionado2);
        Assertions.assertEquals(lance1, lanceAdicionado1);
    }

}
