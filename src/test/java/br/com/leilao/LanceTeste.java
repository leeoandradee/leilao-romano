package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LanceTeste {

    @Test
    public void testarLance() {
        Usuario user1 = new Usuario(1, "Leonardo");
        Lance lance = new Lance(user1, 100.0);

        Assertions.assertEquals(1, lance.getUsuario().getId());
        Assertions.assertEquals("Leonardo", lance.getUsuario().getNome());
        Assertions.assertEquals(100.0, lance.getValorLance());
    }

}
