package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UsuarioTeste {

    @Test
    public void testarUsuario() {
        Usuario user1 = new Usuario(1, "Leonardo");
        Assertions.assertEquals(1, user1.getId());
        Assertions.assertEquals("Leonardo", user1.getNome());
    }

}
