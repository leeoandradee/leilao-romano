package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTeste {

    public Usuario user1 = new Usuario(1, "Leonardo");
    public Lance lance = new Lance(user1, 100.0);
    public Leilao leilao = new Leilao();
    public Leiloeiro leiloeiro = new Leiloeiro("Jonatha", leilao);

    public void setUp() {

    }

    @Test
    public void testarLeiloeiro() {

        Lance lance1 = leilao.adicionarNovoLance(lance);

        Assertions.assertEquals("Jonatha", leiloeiro.getNome());
        Assertions.assertEquals(lance1, lance);
    }

    @Test
    public void testarRetornarMaiorLance() {
        Lance maiorLance = leiloeiro.retornarMaiorLance();
        Assertions.assertEquals(100.0, lance.getValorLance());
        Assertions.assertEquals(user1, lance.getUsuario());
    }




}
